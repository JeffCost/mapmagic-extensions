﻿/**
  @ingroup Random
  @class  Random:SimplexNoise
  @date   09/03/2016
  @author Hellhound

  @brief 
  Implementation a speed-improved simplex noise algorithm for 1D, 2D, 3D.
  
  This implementation use a seed for pseudo random value creation of the 
  permutation lookup table. Using the same seed will always create the same 
  noise results. 
  
  @remark This algorithm was originally designed by Ken Perlin, but my code 
  has been adapted from the implementation written by Stefan Gustavson.
  
  @see http://staffwww.itn.liu.se/~stegu/aqsis/aqsis-newnoise
    	
  @remark A good description of the algorithm itself could be found here.
  @see https://code.google.com/p/fractalterraingeneration/wiki/Simplex_Noise

  Updated :
  Status  : FINAL
  Copyright Binary Revolution, Inc.  All rights reserved.
*/
using System.Linq;
using UnityEngine;
	
namespace Random
{
	public class SimplexNoiseProvider : GradientNoiseProvider
	{		
	    private static int[][] grad3 = new int[][] {
			new int[] {1,1,0}, new int[] {-1,1,0}, new int[] {1,-1,0}, new int[] {-1,-1,0},
            new int[] {1,0,1}, new int[] {-1,0,1}, new int[] {1,0,-1}, new int[] {-1,0,-1},
            new int[] {0,1,1}, new int[] {0,-1,1}, new int[] {0,1,-1}, new int[] {0,-1,-1}
		};
		
		/** 
		@brief Create the Simplex Noise instance based on given seed.
		@note This seed must be > 0 otherwise the actual time stamp of the System.Random 
			  class will be used as default value.       
		@param seed The seed used for random value creation. (Default == 0).
		*/
		public SimplexNoiseProvider(int seed=0)
		:base(seed)
		{}

		/**
		@brief Generate a 1D simplex noise value.
		@param x  The x coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The simplex noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float frequency)
		{
			// noise contributions
			float n0, n1;
			
			int i0 = Mathf.FloorToInt(x * frequency); 
			int i1 = i0 + 1;
			float x0 = x - i0;
			float x1 = x0 - 1.0f;
				   
			float t0 = 1.0f - x0 * x0;
			t0 *= t0;
			n0 = t0 * t0 * grad(permutation[i0 & 0xff], x0);

			float t1 = 1.0f - x1 * x1;
			t1 *= t1;
			n1 = t1 * t1 * grad(permutation[i1 & 0xff], x1);

			// The maximum value of this noise is 8*(3/4)^4 = 2.53125
			// A factor of 0.395 scales to fit exactly within [-1,1]
			return 0.395f * (n0 + n1);
		}

		/**
		@brief Generate a 2D simplex noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The simplex noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float y, float frequency)
		{
			float xin = x*frequency;
			float yin = y*frequency;
			
			// 2D specific factors to (un-)skew the triangle corners 
			float SkewFactor = 0.5f * (Mathf.Sqrt(3f) - 1f);
			float UnskewFactor = (3f - Mathf.Sqrt(3f)) / 6f;
			
			// Noise contributions from the three corners
			float n0, n1, n2; 
			
			/* Skew the input values to determine the bottom left corner
			   of the triangle of the skewed triangle grid*/ 
			float skew = (xin+yin) * SkewFactor;
			int i = Mathf.FloorToInt(xin + skew);
			int j = Mathf.FloorToInt(yin + skew);
			
			//Unskew the corner origin back to (x,y) simplex space.
			float unskew = (i + j) * UnskewFactor;
			float X0 = i - unskew;
			float Y0 = j - unskew;
			
			// Determine the distance from the bottom corner
			float x0 = xin - X0; 
			float y0 = yin - Y0;

			// For the 2D case, the simplex shape is an equilateral triangle.
			// Determine the middle corner in skewed space
			int i1, j1;
			if (x0 > y0) { i1 = 1; j1 = 0; } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
			else { i1 = 0; j1 = 1; }      // upper triangle, YX order: (0,0)->(0,1)->(1,1)	
    
			//Determine the top corner in simplex (unskewed) space
			float x1 = x0 - i1 + UnskewFactor; // Offsets for middle corner in (x,y) unskewed coords
			float y1 = y0 - j1 + UnskewFactor;

			float x2 = x0 - 1.0f + 2.0f * UnskewFactor; // Offsets for last corner in (x,y) unskewed coords
			float y2 = y0 - 1.0f + 2.0f * UnskewFactor;
	
			// Wrap the integer indices at 256, to avoid indexing permutation[] out of bounds
			int ii = i & 255;
			int jj = j & 255;
			
			int gi0 = permutation[ii+permutation[jj]] % 12;
			int gi1 = permutation[ii+i1+permutation[jj+j1]] % 12;
			int gi2 = permutation[ii+1+permutation[jj+1]] % 12;

			// Calculate the contribution from the three corners
			float t0 = 0.5f - x0 * x0 - y0 * y0;
			if (t0 < 0.0f) n0 = 0.0f;
			else {
				t0 *= t0;
				n0 = t0 * t0 * dot(grad3[gi0], x0, y0); 
			}
    
			float t1 = 0.5f - x1 * x1 - y1 * y1;
			if (t1 < 0.0f) n1 = 0.0f;
			else {
				t1 *= t1;
				n1 = t1 * t1 * dot(grad3[gi1], x1, y1);
			}
			
			float t2 = 0.5f - x2 * x2 - y2 * y2;
			if (t2 < 0.0f) n2 = 0.0f;
			else {
				t2 *= t2;
				n2 = t2 * t2 * dot(grad3[gi2], x2, y2); 
			}
			
			// Add contributions from each corner to get the final noise value.
			// The result is scaled to return values in the interval [-1,1].
			return (65.0f * (float)(n0 + n1 + n2));
		}
		
		/**
		@brief Generate a 3D simplex noise value.
		@param x  The x coordinate.
		@param y  The y coordinate.
		@param z  The z coordinate.
		@param frequency The frequency which must be greater than zero.
		@return float The simplex noise value in range [-1,1]. 
		*/
		public override float Generate(float x, float y, float z, float frequency)
		{		
				// 3D specific factors to (un-)skew the triangle corners 
				float SkewFactor = 1.0f/3.0f;
				float UnskewFactor = 1.0f/6.0f;

				// Noise contributions from the four corners
				float n0, n1, n2, n3; 

				x *= (float)frequency;
				y *= (float)frequency;
				z *= (float)frequency;

				/* Skew the input values to determine the bottom left corner
				   of the triangle of the skewed triangle grid */
				float skew = (x+y+z)*SkewFactor;
				float xs = x+skew;
				float ys = y+skew;
				float zs = z+skew;
				int i = Mathf.FloorToInt(xs);
				int j = Mathf.FloorToInt(ys);
				int k = Mathf.FloorToInt(zs);

				/*Unskew the corner origin back to (x,y) simplex space.*/
				float unskew = (float)(i+j+k)*UnskewFactor; 
				float X0 = i-unskew; 
				float Y0 = j-unskew;
				float Z0 = k-unskew;

				// Determine the distance from the corner origin
				float x0 = x-X0; 
				float y0 = y-Y0;
				float z0 = z-Z0;

				// For the 3D case, the simplex shape is a slightly irregular tetrahedron.
				// Determine which simplex we are in.
				int i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
				int i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords

				if(x0>=y0) {
					if(y0>=z0){ i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; } // X Y Z order
					else if(x0>=z0) { i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; } // X Z Y order
					else { i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; } // Z X Y order
					}
				else { // x0<y0
					if(y0<z0) { i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; } // Z Y X order
					else if(x0<z0) { i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; } // Y Z X order
					else { i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; } // Y X Z order
				}

				// Offsets for second corner in (x,y,z) coords
				float x1 = x0 - i1 + UnskewFactor; 
				float y1 = y0 - j1 + UnskewFactor;
				float z1 = z0 - k1 + UnskewFactor;
				
				 // Offsets for third corner in (x,y,z) coords
				float x2 = x0 - i2 + 2.0f*UnskewFactor;
				float y2 = y0 - j2 + 2.0f*UnskewFactor;
				float z2 = z0 - k2 + 2.0f*UnskewFactor;
				
				// Offsets for last corner in (x,y,z) coords
				float x3 = x0 - 1.0f + 3.0f*UnskewFactor; 
				float y3 = y0 - 1.0f + 3.0f*UnskewFactor;
				float z3 = z0 - 1.0f + 3.0f*UnskewFactor;

				// Wrap the integer indices at 256, to avoid indexing permutation[] out of bounds
				int ii = i & 255;
				int jj = j & 255;
				int kk = k & 255;

				// Calculate the contribution from the four corners
				float t0 = 0.6f - x0*x0 - y0*y0 - z0*z0;
				if(t0 < 0.0f) n0 = 0.0f;
				else {
					t0 *= t0;
					n0 = t0 * t0 * grad(permutation[ii+permutation[jj+permutation[kk]]], x0, y0, z0);
				}

				float t1 = 0.6f - x1*x1 - y1*y1 - z1*z1;
				if(t1 < 0.0f) n1 = 0.0f;
				else {
					t1 *= t1;
					n1 = t1 * t1 * grad(permutation[ii+i1+permutation[jj+j1+permutation[kk+k1]]], x1, y1, z1);
				}

				float t2 = 0.6f - x2*x2 - y2*y2 - z2*z2;
				if(t2 < 0.0f) n2 = 0.0f;
				else {
					t2 *= t2;
					n2 = t2 * t2 * grad(permutation[ii+i2+permutation[jj+j2+permutation[kk+k2]]], x2, y2, z2);
				}

				float t3 = 0.6f - x3*x3 - y3*y3 - z3*z3;
				if(t3<0.0f) n3 = 0.0f;
				else {
					t3 *= t3;
					n3 = t3 * t3 * grad(permutation[ii+1+permutation[jj+1+permutation[kk+1]]], x3, y3, z3);
				}

				// Add contributions from each corner to get the final noise value.
				// The result is scaled to stay just inside [-1,1]
				return 32.0f * (n0 + n1 + n2 + n3); 				
		}
		
		private float grad( int hash, float x )
		{
			int h = hash & 15;
			float grad = 1.0f + (h & 7);   // Gradient value 1.0, 2.0, ..., 8.0
			if ((h & 8) != 0) grad = -grad;         // Set a random sign for the gradient
			return ( grad * x );           // Multiply the gradient with the distance
		}

		private float grad(int hash, float x, float y)
		{
			int h = hash & 7;      // Convert low 3 bits of hash code
			float u = h < 4 ? x : y;  // into 8 simple gradient directions,
			float v = h < 4 ? y : x;  // and compute the dot product with (x,y).
			return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0f * v : 2.0f * v);
		}

		private float grad(int hash, float x, float y, float z)
		{
			int h = hash & 15;     // Convert low 4 bits of hash code into 12 simple
			float u = h < 8 ? x : y; // gradient directions, and compute dot product.
			float v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
			return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
		}
		
		private float dot(int[] g, float x, float y) {
			return g[0]*x + g[1]*y; 
		}
	}
}