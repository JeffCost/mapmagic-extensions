Public domain extensions for the Unity procedural terrain generation tool Map Magic: http://www.denispahunov.ru/MapMagic/

You have only to extract the included files in the main folder of the Map Magic Plug-in. Feel free to use, extend or modify it. If you found a bug or general issue I'm deeply thankful if you report an issue at this repository. 